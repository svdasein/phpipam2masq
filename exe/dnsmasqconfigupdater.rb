#!/usr/bin/env ruby

require 'sinatra'

set :bind, '0.0.0.0'

get '/updateDnsmasq' do
	output = String.new
	output += Time.now.to_s
	output += "\n#{%x(phpipam2masq.rb 2>&1)}"
	output += Time.now.to_s
	output += "\n#{%x(systemctl restart dnsmasq 2>&1 ; echo "Restart exit: $?")}"
	output += Time.now.to_s
	%Q(<pre>#{output}</pre>)
end

