#!/usr/bin/env ruby

require 'faraday'
require 'json'
require 'trollop'
require 'facets'
require 'awesome_print'
require 'ipaddress'
require 'yaml'

defaults = {
    :config => '/etc/phpipam2masq.conf',
    :destdir => '/etc',
    :apiurl => 'http://localhost/api',
    :apiuser=> '',
    :apipass => '',
    :section => '0',
    :dhcptag => '4'
}

opts = Trollop::options do
  usage '[options]'
  #version %Q{Version #{Gestio2masq::VERSION}}
  opt :config, 'Config file path', :short => 'c', :type => :string
  opt :destdir, 'Destination dir', :short => 't', :type => :string
  opt :apiurl, 'API URL', :short => 'a', :type => :string
  opt :apiuser, 'Database user', :short => 'u', :type => :string
  opt :apipass, 'Database password', :short => 'p', :type => :string
  opt :sections , "Section IDs (comma sep)", :short => 's', :type=>:string
  opt :dhcptag , "DHCP Tag value", :short => 'd', :type=>:integer
  opt :verbose, 'Verbose output', :short => 'v'
end

config_file = opts[:config]||defaults[:config]

config = nil
if File.exist?(config_file)
  begin
    config = YAML.load(File.read(config_file))
  rescue => detail
    puts detail.backtrace
    puts %Q(Error reading config from file #{config_file})
    exit(1)
  end
end

if config.nil? or (config.class != Hash)
  config = Hash.new
else
  # symbolize hash keys (json format compatibility)
  config = Hash[config.map { |(k, v)| [k.to_sym, v] }]
end

# Precedence (low to high): defaults, config file, command line
config = defaults.merge(config).merge(Hash(opts.select { |k, v| !v.nil? }))


conn = Faraday.new(:url=>config[:apiurl])
conn.basic_auth(config[:apiuser],config[:apipass])
auth = conn.post('user/')
auth = JSON.parse(auth.body)
conn.headers['token'] = auth['data']['token']

subnets = Hash.new
config[:sections].split(',').each { |subnetid|
	JSON.parse(conn.get("sections/#{subnetid}/subnets/").body)['data'].each  { |subnet|
		subnets[subnet['id']] = subnet.symbolize_keys
	}
}

addresses = Hash.new
subnets.keys.each { |subnetid|
	resp = JSON.parse(conn.get("subnets/#{subnetid}/addresses/").body)
	if resp.has_key?('data')
		resp['data'].each { |addr|
			addr.symbolize_keys!
			if not addr[:id].nil?
				addresses[addr[:id]] = addr
				addresses[addr[:id]][:subnet] = subnets[subnetid]
			end
		}
	end
}


# Convert all the addreses keys to symbols

hosts_entries = Array.new
dhcp_hosts_entries = Array.new
cnames_entries = Array.new
addresses.values.each { |host|
  if host[:tag] != config[:dhcptag] and not host[:hostname].nil? and host[:hostname].size > 0
    if host[:mac] and host[:mac].size > 0
      # static dhcp assignment
      if host[:optiontag]
        dhcp_hosts_entries.push("#{host[:mac]},set:#{host[:optiontag]},#{host[:hostname].split('.').shift},#{host[:ip]},#{host[:subnet][:leasetime]}")
      else
        dhcp_hosts_entries.push("#{host[:mac]},#{host[:hostname].split('.').shift},#{host[:ip]},#{host[:subnet][:leasetime]}")
      end
    end
    # hostname
    if host[:subnet] and host[:subnet][:Domain]
      hosts_entries.push("#{host[:ip]}\t#{host[:hostname]}.#{host[:subnet][:Domain]}")
    else
      hosts_entries.push("#{host[:ip]}\t#{host[:hostname]}")
    end
    # cnames
    if host[:CNAMEs]
      host[:CNAMEs].split(',').each { |cname|
        if host[:subnet] and host[:subnet][:Domain]
          cnames_entries.push("cname=#{cname},#{host[:hostname]}.#{host[:subnet][:Domain]}")
        else
          cnames_entries.push("cname=#{cname},#{host[:hostname]}")
        end
      }
    end
  end
}

# This monster produces a hash, one kv for each subnet, where the v contains an array of ranges for the subnet in question.  This works
# under the presumption that the phpipam api produces an ordered list of addresses when one dumps all addresses from a subnet (which
# as of this writing appears to be true)
subnetranges = Hash.new
subnets.keys.each { |subnetid|
	subnetranges[subnetid] = Array.new
	rawrange = addresses.values.select {|each| each[:subnet][:id] == subnetid}.chunk {|each| each[:tag] == config[:dhcptag]}.select {|isDhcp,ary| isDhcp}
	if rawrange.size > 0
		# This next one ensures that we're dealing with immediately adjacent ip addresses by looking a the last octet of two in the list and
		# seeing if they're immediately next to each other. If they are not we have multiple chunks in this subnet.
		ranges = rawrange[0][1].chunk_while { |a,b| a[:ip].split('.').pop.to_i + 1 == b[:ip].split('.').pop.to_i}
		ranges.to_a.each {|range|
			subnetranges[subnetid].push(range)
		}
	end
}


ranges_entries = Array.new
subnetranges.each_pair { |subnetid,ranges|
	subnet = subnets[subnetid]
	subnettag = subnet[:subnettag]
	next if not subnettag or ranges.size == 0
	ranges.each { |range|
		first = range.first
		last = range.last
		mask = IPAddress.parse("#{subnet[:subnet]}/#{subnet[:mask]}").netmask
		ranges_entries.push("dhcp-range=#{subnettag},#{first[:ip]},#{last[:ip]},#{mask},#{subnet[:dynleasetime]}")
	}
}


puts "="*80 if config[:verbose]
puts "RANGES: #{ranges_entries.size}"
File.new(%Q(#{config[:destdir]}/dnsmasq.d/ranges), 'w').puts(ranges_entries.join("\n"))
puts(ranges_entries.join("\n")) if config[:verbose]
puts "="*80 if config[:verbose]
puts "CNAMES: #{cnames_entries.size}"
File.new(%Q(#{config[:destdir]}/dnsmasq.d/cnames), 'w').puts(cnames_entries.join("\n"))
puts(cnames_entries.join("\n")) if config[:verbose]
puts "="*80 if config[:verbose]
puts "DHCP HOSTS: #{dhcp_hosts_entries.size}"
File.new(%Q(#{config[:destdir]}/dnsmasq-dhcp-hosts.conf), 'w').puts(dhcp_hosts_entries.join("\n"))
puts(dhcp_hosts_entries.join("\n")) if config[:verbose]
puts "="*80 if config[:verbose]
puts "HOSTS: #{hosts_entries.size}"
File.new(%Q(#{config[:destdir]}/dnsmasq-hosts.conf), 'w').puts(hosts_entries.join("\n"))
puts(hosts_entries.join("\n")) if config[:verbose]


