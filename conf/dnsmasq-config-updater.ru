require 'rubygems'
version = ">= 0.a"
gem 'phpipam2masq', version
require Gem.bin_path('phpipam2masq', 'dnsmasqconfigupdater.rb', version)
run Sinatra::Application
