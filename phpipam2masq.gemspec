# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'phpipam2masq/version'

Gem::Specification.new do |spec|
  spec.name          = "phpipam2masq"
  spec.version       = Phpipam2masq::VERSION
  spec.authors       = ["Dave Parker"]
  spec.email         = ["daveparker01@gmail.com"]

  spec.summary       = %q{Logic to push phpipam configuration to a dnsmasq configuration}
  spec.homepage      = "https://gitlab.com/svdasein/phpipam2masq"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https://rubygems.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.9"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_runtime_dependency 'faraday', '~> 0.15'
  spec.add_runtime_dependency 'json', '~> 2.0'
  spec.add_runtime_dependency 'trollop', '~> 2.1'
  spec.add_runtime_dependency 'facets', '~> 3.0'
  spec.add_runtime_dependency 'awesome_print', '~> 1.0'
  spec.add_runtime_dependency 'ipaddress', '~> 0.8'
  spec.add_runtime_dependency 'sinatra', '~> 2.0'
end
