# phpipam2masq

phpipam2masq is a small application that generates dnsmasq configuration files from a [phpipam](https://phpipam.net/) instance.  It's appropriate for SOHO use as it is and can (with some hacking) be made to work "ok" in a larger environment.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'phpipam2masq'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install phpipam2masq

## Configuration

Before using it, you must create a new api key in phpipam for the application:

![API Key screenshot](img/apikey.png)

You should also add a few custom fields to your phpipam installation:

![Custom Fields screenshot](img/customfields.png)

On your dnsmasq system (where you'll be running phpipam2masq), you need to make a configration
destination directory.  It should look like this:

	dest/
	dest/dnsmasq.d/

If you take the defaults (see below), dest is /etc and dnsmasq.d will likely already be there.

The following files will be generated:

	dest/dnsmasq.d/ranges
	dest/dnsmasq.d/cnames
	dest/dnsmasq-hosts.conf
	dest/dnsmasq-dhcp-hosts.conf

Finally, configure dnsmasq to point at the generated files:

	addn-hosts=dest/dnsmasq-hosts.conf
	dhcp-hostsfile=dest/dnsmasq-dhcp-hosts.conf
	conf-dir=dest/dnsmasq.d
 
## Usage

phpipam2masq options can be specified entirely on the command line or in a configuration file.  Command line switches override config file settings.

    Usage: phpipam2masq [options]

    Options:
    -c, --config=<s>      Config file path
    -t, --destdir=<s>     Destination dir
    -a, --apiurl=<s>      API URL
    -u, --apiuser=<s>     Database user
    -p, --apipass=<s>     Database password
    -s, --sections=<s>    Section IDs (comma sep)
    -d, --dhcptag=<i>     DHCP Tag value
    -v, --verbose         Verbose output
    -h, --help            Show this message

Default values are:

	defaults = {
	    :config => '/etc/phpipam2masq.conf',
	    :destdir => '/etc',
	    :apiurl => 'http://localhost/api',
	    :apiuser=> '',
	    :apipass => '',
	    :section => '0',
	    :dhcptag => '4'
	}

The configuration file format is yaml:

	---
	apiurl: http://myhost/phpipam/api/masq (or the app name you chose)/
	apiuser: myuser
	apipass: mypass
	sections: "<csv list of phpipam section numbers>"

phpipam2masq re-generates all the files listed above each time you execute it.

I've included a very slapped together web interface for this thing that you can use to update your dnsmasq configuration from a browser.  See the .ru in conf/ and the corresponding thing in exe/.  It's not secure and it's not elegant but it does the job for my home lab.  Feel free to embellish it.  I put a link to this in my phpipam "Instructions" page that pops up a little window that shows command output - very handy.


## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/svdasein/phpipam2masq.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

